<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\UlidValidator;

/* php bin/console doctrine:fixtures:load pour charger les fixtures */
/* php bin/console cache:clear */

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR'); // Attention \ avant Faker car dans namespace global !!!

        // On va créer 5 utilisateurs
        for($i=1 ; $i<5 ; $i++){
            $user= new User();
            $user->setUsername($faker->word());
            $user->setPassword($this->passwordHasher->hashPassword($user,'testtest'));
            $user->setRoles(["ROLE_USER"]);
            $user->setFirstname($faker->firstName());
            $user->setLastname($faker->lastName());

            $email = strtolower($user->getFirstname() . '.' . $user->getLastname() . '@gmail.com');
            $user->setEmail($email);

            $manager->persist($user);

            // On crée 3 articles pour chaque utilisateur
            for($j=0 ; $j<3 ; $j++){
                $article = new Article();
                $article->setTitle($faker->sentence());
                $article->setCreationDate($faker->dateTime());
                $article->setImage($faker->imageUrl(640,480,null,false,null,true));
                $article->setContent($faker->paragraph(random_int(50,150)));
                $article->setUser($user);

                $manager->persist($article);

                for($k=0; $k < random_int(1,4) ; $k++){
                    $comment=new Comment();
                    $comment->setContent($faker->paragraph(3));
                    $comment->setCreationDate($faker->dateTime()); // TODO : Après date création article !!
                    $comment->setArticle($article);
                    $comment->setUser($user);

                    $manager->persist($comment);
                }
            }

        }

        //Création d'un compte admin
        $user= new User();
        $user->setUsername('bibi');
        $user->setPassword($this->passwordHasher->hashPassword($user,'bibibibi'));
        $user->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        $user->setFirstname('Bryan');
        $user->setLastname('LEGRAS');

        $email = strtolower($user->getFirstname() . '.' . $user->getLastname() . '@gmail.com');
        $user->setEmail($email);

        $manager->persist($user);

        
        $manager->flush();
    }
}
