<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class CommentController extends AbstractController
{
    /**
     * @Route("/edit/comment/{id}",name="editComment")
     */
    public function index(Request $request, Comment $comment, EntityManagerInterface $manager): Response
    {

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setModificationDate(new \DateTime());

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('article',['id'=>$comment->getArticle()->getId()]);
        }

        return $this->render('comment/index.html.twig', [
            'comment' => $comment,
            'commentForm' => $form->createView()
        ]);
    }
}
