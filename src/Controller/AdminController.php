<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
    * @Route("/admin/users", name="admin_users")
    */
    public function users(UserRepository $usersRepo): Response
    {
        $users = $usersRepo->findAll();

        return $this->render('admin/users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
    * @Route("/admin/users/{id}", name="admin_edit_user")
    */
    public function editUser(Request $request, User $user, EntityManagerInterface $manager, UserPasswordHasherInterface $passwordHasher): Response
    {
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $user->setPassword($passwordHasher->hashPassword($user,$user->getPassword()));

            $manager->persist($user);
            $manager->flush();
    
            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/editUser.html.twig', [
            'user' => $user,
            'userForm' => $form->createView(),
        ]);
    }
}
